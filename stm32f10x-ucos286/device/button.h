/******************************************************************************
 * This file is part of embedme-mcu.
 *
 * embedme-mcu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * embedme-mcu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with embedme-mcu.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: embedme-mcu
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme-mcu
 * Copyright 2013~2019 @ ShenZhen ,China
*******************************************************************************/
#ifndef __BUTTON_H__
#define __BUTTON_H__

#include "basetype.h"
#include "kernel.h"

typedef enum{
    BTN_ID_KEY1=0,
    BTN_ID_KEY2,
    BTN_ID_MAX,
}BTN_ID_E;

typedef enum{
    BTN_EVT_UP=0,/* ̧�� */
    BTN_EVT_DOWN,/* ���� */
}BTN_EVT_E;

typedef struct{
    uint8 m_id;
    uint8 m_evt;
}ButtonEvent_S;

bool button_init(uint8 buttonID,GpioInfo_S gpioInfo);
void button_scan(OS_EVENT* evtMsgQueue);

#endif
