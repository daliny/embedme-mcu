/******************************************************************************
 * This file is part of embedme-mcu.
 *
 * embedme-mcu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * embedme-mcu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with embedme-mcu.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: embedme-mcu
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme-mcu
 * Copyright 2013~2019 @ ShenZhen ,China
*******************************************************************************/
#include "board.h"
#include "ad5160.h"
#include "spi.h"
#include "basetype.h"
#include "trace.h"

static uint8 s_ad5160_reg_table[AD5160_VOLUME_MAX]={
#if 0 /* 理论值 */
    0x00,
    0x11,
    0x33,
    0x55,
    0x77,
    0x99,
    0xbb,
    0xdd,
    0xff
#else
    0x05,   /* 0级(0灯) */
    0x05,   /* 1级(0灯) */
    0x15,   /* 2级(1灯) */
    0x34,   /* 3级(1灯) */
    0x62,   /* 4级(2灯) */
    0x96,   /* 5级(2灯) */
    0xc3,   /* 6级(3灯) */
    0xe5,   /* 7级(3灯) */
    0xfd,   /* 8级(4灯) */
    0xfd,   /* 9级(4灯) */
#endif
};

static sint8 s_volume_level=0;

void ad5160_init()
{
    //s_volume_level = g_appConfig.m_volumeLevel;//读取之前的数据
    if (s_volume_level<0 || s_volume_level>=AD5160_VOLUME_MAX)
    {
        /* 如果Flash值不对(),则设置为默认值 */
        s_volume_level = AD5160_VOLUME_DEFAULT;
    }
    SPI_OPEN(SPI_DEV_AD5160);
}

sint8 ad5160_get_volume()
{
    return s_volume_level;
}

void ad5160_set_volume(sint8 level)
{
    if (level<0 || level>=AD5160_VOLUME_MAX)
    {
        TRACE_ERR("invalid volume:%d!",level);
        return;
    }
    uint8 value=s_ad5160_reg_table[level];
    if(0==SPI_WRITE(SPI_DEV_AD5160,value))
    {
        TRACE_REL("set volume-[%d][0x%x] OK.",level,value);
    }
    else
    {
        TRACE_ERR("set volume-[%d][0x%x] ERROR!",level,value);
    }
}

void ad5160_volume_up()
{
    s_volume_level++;
    if (s_volume_level>=AD5160_VOLUME_MAX-2)
    {
        s_volume_level=AD5160_VOLUME_MAX-1;
    }
    ad5160_set_volume(s_volume_level);
}

void ad5160_volume_down()
{
    s_volume_level--;
    if (s_volume_level<2)
    {
        s_volume_level=0;
    }
    ad5160_set_volume(s_volume_level);
}