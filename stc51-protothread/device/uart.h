/******************************************************************************
 * This file is part of embedme-mcu.
 *
 * embedme-mcu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * embedme-mcu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with embedme-mcu.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: embedme-mcu
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme-mcu
 * Copyright 2013~2019 @ ShenZhen ,China
*******************************************************************************/
#ifndef __UART_H__
#define __UART_H__

#include "basetype.h"
#include "platform.h"

typedef enum
{
    UART_COM0=0,
    UART_COM1,
    UART_COM2
}UART_ID;

bool uart_init(uint8 uart);
bool uart_send_byte(uint8 uart, char ch);
bool uart_recv_byte(uint8 uart, char* pch);
void uart_send_str(uint8 uart,const char* str);
void uart_format_value(uint8 uart,uint8 value,uint8 radix);

#endif
