/******************************************************************************
 * This file is part of embedme-mcu.
 *
 * embedme-mcu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * embedme-mcu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with embedme-mcu.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: embedme-mcu
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme-mcu
 * Copyright 2013~2019 @ ShenZhen ,China
*******************************************************************************/
#ifndef __HJBOARD_H__
#define __HJBOARD_H__
/**********************************************************
 File   : hjboard.h
 Bref   : 慧净开发板
 Version: V1.0.0
 Author : FergusZeng
 Date   : 2013-12-08
***********************************************************/
#include "platform.h"
#include "uart.h"

#define COM_DEBUG   UART_COM0

/* 按键 */
#define KEY_K1      (P3_4)
#define KEY_K2      (P3_5)
#define KEY_K3      (P3_6)
#define KEY_K4      (P3_7)

/*---------------------------------------------
               4X4按键
  P3.4 P3.5 P3.6 P3.7
  |    |    |    |
  S1---S2---S3---S4----P3.0
  |    |    |    |
  S5---S6---S7---S8----P3.1
  |    |    |    |
  S9---S10--S11--S12---P3.2
  |    |    |    |
  S13--S14--S15--S16---P3.3
  --------------------------------------------*/
#define KSCAN_X0        (P3_4)      /* 水平0 */
#define KSCAN_X1        (P3_5)      /* 水平1 */
#define KSCAN_X2        (P3_6)      /* 水平2 */
#define KSCAN_X3        (P3_7)      /* 水平3 */
#define KSCAN_Y0        (P3_0)      /* 垂直0 */
#define KSCAN_Y1        (P3_1)      /* 垂直1 */
#define KSCAN_Y2        (P3_2)      /* 垂直2 */
#define KSCAN_Y3        (P3_3)      /* 垂直3 */

/*  LED灯  */
#define LED_L0          (P1_0)    
#define LED_L1          (P1_1)
#define LED_L2          (P1_2)
#define LED_L3          (P1_3)
#define LED_L4          (P1_4)
#define LED_L5          (P1_5)
#define LED_L6          (P1_6)
#define LED_L7          (P1_7)

/* I2C */
#define I2C_SDA         (P2_0)
#define I2C_SCL         (P2_1)

/* DS1302 RTC */
#define RTC_SDIO        (P2_0)
#define RTC_SCLK        (P2_1)
#define RTC_RST         (P2_4)

/* 数据GPIO */
#define GPIO_D0         (P0_0)
#define GPIO_D1         (P0_1)
#define GPIO_D2         (P0_2)
#define GPIO_D3         (P0_3)
#define GPIO_D4         (P0_4)
#define GPIO_D5         (P0_5)
#define GPIO_D6         (P0_6)
#define GPIO_D7         (P0_7)

#define GPIO_DB1        (P1_0)
#define GPIO_DB2        (P1_1)
#define GPIO_DB3        (P1_2)
#define GPIO_DB4        (P1_3)
#define GPIO_DB5        (P1_4)
#define GPIO_DB6        (P1_5)
#define GPIO_DB7        (P1_6)
#define GPIO_DB8        (P1_7)

#define GPIO_18B20      (P2_2)  /* 温度传感器 */
#define GPIO_FM         (P2_3)
#define GPIO_DU         (P2_6)  /* 数码管段选 */
#define GPIO_WE         (P2_7)  /* 数码管位选 */
#define GPIO_LCDEN      (P2_5)

#define GPIO_INT1       (P3_3)

void hjboard_set_alarm(bool enable);
void hjboard_set_i2c(bool enable);

#endif
