/******************************************************************************
 * This file is part of embedme-mcu.
 *
 * embedme-mcu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * embedme-mcu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with embedme-mcu.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: embedme-mcu
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme-mcu
 * Copyright 2013~2019 @ ShenZhen ,China
*******************************************************************************/
#include "i2c.h"
/* 定义通用结构体 */
typedef struct{
    uint32 m_gpioSCL;
    uint32 m_gpioSDA;
    bool m_initFlag;
}I2cBus_S;

#if USE_I2C_BASED_ON_GPIO
#include "i2c_gpio.c"
#else
#include "board.h"
#include "gpio.h"
#include "trace.h"
#include "kernel.h"
#include "mymutex.h"

static I2cBus_S s_i2cBus[I2C_BUS_MAX]={
      /* SCL  SDA  initFlag */
      {PB(6),PB(7),0},
      {0    ,0    ,0}
};

#define I2C_RW_TIMEOUT      50     /* i2c读写超时时间:50个时钟周期 */

static I2C_TypeDef* get_i2c(uint8 i2c_bus)
{
    switch(i2c_bus){
    case I2C_BUS_0:
        if(!s_i2cBus[i2c_bus].m_initFlag)
        {
            RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C1, ENABLE);
        }
        return I2C1;
    case I2C_BUS_1:
        if(!s_i2cBus[i2c_bus].m_initFlag)
        {
            RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C2, ENABLE);
        }
        return I2C2;
    default:
        break;
    }
    return NULL;
}

static sint32 i2c_soc_reset_bus(uint8 i2c_bus,sint32 timeout_us)
{
    sint32 timeout=0;
    uint16 state;
    switch(i2c_bus){
    case I2C_BUS_0:
    {
        state = I2C_ReadRegister(I2C1, I2C_Register_SR1);
        while(state&0x2)
        {
            if (timeout<timeout_us)
            {                
                GPIO_INIT(s_i2cBus[i2c_bus].m_gpioSCL,GPIO_Mode_AF_OD,GPIO_Speed_2MHz);
                GPIO_INIT(s_i2cBus[i2c_bus].m_gpioSDA,GPIO_Mode_AF_OD,GPIO_Speed_2MHz);
                GPIO_SET_VAL(s_i2cBus[i2c_bus].m_gpioSCL,0);
                udelay(2);
                I2C_SoftwareResetCmd(I2C1, ENABLE);
                I2C_ClearFlag(I2C1, I2C_FLAG_AF);
                state = I2C_ReadRegister(I2C1, I2C_Register_SR1);
                timeout += 2;
            }
            else
            {
                return API_TIMEOUT;
            }
        }
        return API_OK;
    }
    case I2C_BUS_1:
    {
        break;
    }
    default:
        return API_ERROR;
    }
}

bool i2c_soc_init(uint8 i2c_bus,uint32 speed)
{
    if (i2c_bus>=I2C_BUS_MAX)
    {
        TRACE_ERR("Invalid i2c bus:%d!",i2c_bus);
        return false;
    }
    if (!s_i2cBus[i2c_bus].m_initFlag)
    {
        switch(i2c_bus){
        case I2C_BUS_0:/* I2C1 */
        {
            I2C_InitTypeDef i2cInit;
            GPIO_INIT(s_i2cBus[i2c_bus].m_gpioSCL,GPIO_MODE_AF_OD,GPIO_SPEED_2MHZ);
            GPIO_INIT(s_i2cBus[i2c_bus].m_gpioSDA,GPIO_MODE_AF_OD,GPIO_SPEED_2MHZ);
            I2C_StructInit(&i2cInit);
            i2cInit.I2C_Ack = I2C_Ack_Enable;
            i2cInit.I2C_ClockSpeed = speed;
            I2C_DeInit(get_i2c(i2c_bus));
            I2C_Cmd(get_i2c(i2c_bus), ENABLE);
            I2C_Init(get_i2c(i2c_bus), &i2cInit);
            I2C_AcknowledgeConfig(get_i2c(i2c_bus), ENABLE);
            break;
        }
        case I2C_BUS_1:
        {
            break;
        }
        default:
            return false;
        }
        s_i2cBus[i2c_bus].m_initFlag = true;
        TRACE_REL("Init soc i2c_bus:%d ok,speed:%d...",i2c_bus,speed);
    }
    return true;
}

uint32 i2c_soc_read(uint8 i2c_bus, uint8 i2c_dev, uint8* wdata, uint32 wlen, uint8* rdata,  uint32 rlen)
{
    uint32 i;
    uint8 step=0;
    I2C_TypeDef* i2c = get_i2c(i2c_bus);
    WAIT_ON(I2C_GetFlagStatus(i2c, I2C_FLAG_BUSY)==SET,I2C_RW_TIMEOUT);
    if (wlen > 0)
    {
        step=1;
        I2C_GenerateSTART(i2c, ENABLE);
        //WAIT_ON(I2C_CheckEvent(i2c_bus, I2C_EVENT_MASTER_MODE_SELECT)==RESET,I2C_RW_TIMEOUT);
        WAIT_ON(I2C_GetFlagStatus(i2c, I2C_FLAG_SB)==RESET,I2C_RW_TIMEOUT);
        step=2;
        I2C_Send7bitAddress(i2c, i2c_dev, I2C_Direction_Transmitter);
        WAIT_ON(I2C_CheckEvent(i2c, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED)==RESET,I2C_RW_TIMEOUT);
        step=3;
        for (i=0; i<wlen; i++)
        {
            I2C_SendData(i2c, wdata[i]);
            WAIT_ON (I2C_CheckEvent(i2c, I2C_EVENT_MASTER_BYTE_TRANSMITTED)==RESET,I2C_RW_TIMEOUT);
        }
    }
    if (rlen > 1)
    {
        I2C_AcknowledgeConfig(i2c, ENABLE);
    }
    else
    {
        I2C_AcknowledgeConfig(i2c, DISABLE);
    }
    step=4;
    I2C_GenerateSTART(i2c, ENABLE);
    WAIT_ON(I2C_GetFlagStatus(i2c, I2C_FLAG_SB)==RESET,I2C_RW_TIMEOUT);
    step=5;
    I2C_Send7bitAddress(i2c, i2c_dev, I2C_Direction_Receiver);
    WAIT_ON(I2C_CheckEvent(i2c, I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED)==RESET,I2C_RW_TIMEOUT);
    step=6;
    i = 0;
    while (rlen > 0)
    {
        WAIT_ON(I2C_CheckEvent(i2c, I2C_EVENT_MASTER_BYTE_RECEIVED)==RESET,I2C_RW_TIMEOUT);
        if (rlen == 2)
        {
            I2C_AcknowledgeConfig(i2c, DISABLE);
        }
        else if (rlen == 1)
        {
            I2C_GenerateSTOP(i2c, ENABLE);
        }
        rdata[i++] = I2C_ReceiveData(i2c);
        rlen --;
    }
    return i;
TIMEOUT:
    TRACE_ERR("i2c_readx <0x%02x> [0x%02x] (%x),step:%d\n", i2c_dev, wdata[0],I2C_GetLastEvent(i2c),step);
    I2C_GenerateSTOP(i2c, ENABLE);
    return 0;
}

uint32 i2c_soc_write(uint8 i2c_bus, uint8 i2c_dev, uint8 * data, uint32 len)
{
    uint32 i=0;
    uint8 step=0;
    I2C_TypeDef* i2c = get_i2c(i2c_bus);
    WAIT_ON(I2C_GetFlagStatus(i2c, I2C_FLAG_BUSY)==SET,I2C_RW_TIMEOUT);

    step=1;
    I2C_GenerateSTART(i2c, ENABLE);
    WAIT_ON(I2C_GetFlagStatus(i2c, I2C_FLAG_SB)==RESET,I2C_RW_TIMEOUT);

    step=2;
    I2C_Send7bitAddress(i2c, i2c_dev, I2C_Direction_Transmitter);
    WAIT_ON(I2C_CheckEvent(i2c, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED)==RESET,I2C_RW_TIMEOUT);

    step=3;
    for (i=0; i<len; i++)
    {
        I2C_SendData(i2c, data[i]);
        WAIT_ON (I2C_CheckEvent(i2c, I2C_EVENT_MASTER_BYTE_TRANSMITTED)==RESET,I2C_RW_TIMEOUT);
    }
    I2C_GenerateSTOP(i2c, ENABLE);
    return i;

TIMEOUT:
    TRACE_ERR("i2c_writex <0x%02x> [0x%02x] (%x),step:%d", i2c_dev,data[0], I2C_GetLastEvent(i2c),step);
    I2C_GenerateSTOP(i2c, ENABLE);
    return 0;
}

uint32 i2c_soc_read_reg(uint8 i2c_bus, uint8 i2c_dev, uint8 reg, uint8* value)
{
    return i2c_soc_read(i2c_bus,i2c_dev, &reg, 1, value, 1);
}

uint32 i2c_soc_write_reg(uint8 i2c_bus,uint8 i2c_dev, uint8 reg, uint8 value)
{
    uint8 obuf[2];
    obuf[0] = reg;
    obuf[1] = value;
    return i2c_soc_write(i2c_bus,i2c_dev, obuf, 2)-1;
}
#endif

